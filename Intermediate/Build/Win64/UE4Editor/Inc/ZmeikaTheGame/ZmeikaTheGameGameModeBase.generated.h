// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZMEIKATHEGAME_ZmeikaTheGameGameModeBase_generated_h
#error "ZmeikaTheGameGameModeBase.generated.h already included, missing '#pragma once' in ZmeikaTheGameGameModeBase.h"
#endif
#define ZMEIKATHEGAME_ZmeikaTheGameGameModeBase_generated_h

#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_SPARSE_DATA
#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_RPC_WRAPPERS
#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZmeikaTheGameGameModeBase(); \
	friend struct Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaTheGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ZmeikaTheGame"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaTheGameGameModeBase)


#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAZmeikaTheGameGameModeBase(); \
	friend struct Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AZmeikaTheGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ZmeikaTheGame"), NO_API) \
	DECLARE_SERIALIZER(AZmeikaTheGameGameModeBase)


#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaTheGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaTheGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaTheGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaTheGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaTheGameGameModeBase(AZmeikaTheGameGameModeBase&&); \
	NO_API AZmeikaTheGameGameModeBase(const AZmeikaTheGameGameModeBase&); \
public:


#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZmeikaTheGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZmeikaTheGameGameModeBase(AZmeikaTheGameGameModeBase&&); \
	NO_API AZmeikaTheGameGameModeBase(const AZmeikaTheGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZmeikaTheGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZmeikaTheGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZmeikaTheGameGameModeBase)


#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_12_PROLOG
#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_SPARSE_DATA \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_RPC_WRAPPERS \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_INCLASS \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_SPARSE_DATA \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ZMEIKATHEGAME_API UClass* StaticClass<class AZmeikaTheGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ZmeikaTheGame_Source_ZmeikaTheGame_ZmeikaTheGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
