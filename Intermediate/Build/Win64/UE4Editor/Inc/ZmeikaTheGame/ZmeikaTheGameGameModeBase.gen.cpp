// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ZmeikaTheGame/ZmeikaTheGameGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeZmeikaTheGameGameModeBase() {}
// Cross Module References
	ZMEIKATHEGAME_API UClass* Z_Construct_UClass_AZmeikaTheGameGameModeBase_NoRegister();
	ZMEIKATHEGAME_API UClass* Z_Construct_UClass_AZmeikaTheGameGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ZmeikaTheGame();
// End Cross Module References
	void AZmeikaTheGameGameModeBase::StaticRegisterNativesAZmeikaTheGameGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AZmeikaTheGameGameModeBase_NoRegister()
	{
		return AZmeikaTheGameGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ZmeikaTheGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ZmeikaTheGameGameModeBase.h" },
		{ "ModuleRelativePath", "ZmeikaTheGameGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AZmeikaTheGameGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::ClassParams = {
		&AZmeikaTheGameGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AZmeikaTheGameGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AZmeikaTheGameGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AZmeikaTheGameGameModeBase, 3733959311);
	template<> ZMEIKATHEGAME_API UClass* StaticClass<AZmeikaTheGameGameModeBase>()
	{
		return AZmeikaTheGameGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AZmeikaTheGameGameModeBase(Z_Construct_UClass_AZmeikaTheGameGameModeBase, &AZmeikaTheGameGameModeBase::StaticClass, TEXT("/Script/ZmeikaTheGame"), TEXT("AZmeikaTheGameGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AZmeikaTheGameGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
